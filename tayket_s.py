import json
import random
import uuid

from flask import request
from pymongo import MongoClient
from flask import Flask

from conf import config
from tools import is_valid_color, is_valid_turtles, is_valid_role

app = Flask(__name__)

# Mongo db handler, in real world it will be inside it's own class
mongo_client = MongoClient(config.MONGODB_CONNECTION_STRING)
character_collection = mongo_client.get_database(config.MONGODB_NAME).get_collection(config.CHARACTERS_COLLECTION)
ninja_jobs_collection = mongo_client.get_database(config.MONGODB_NAME).get_collection(config.JOBS_COLLECTION)


@app.route('/cowabunga', methods=['GET'])
def cowabunga():
    """
    @summary: Get turtle name and prints cowabunga
    """
    req_json = request.get_json()
    if req_json:
        turtle_name = req_json.get('turtle_name')
        if turtle_name:
            return "Cowabunga {0}".format(turtle_name)

    return "No turtle name received, reveied json: {0}".format(req_json)


@app.route('/multi_turtle', methods=['GET'])
def multi_turtle():
    """
    @summary: Get turtle name and prints it as many as the user wants
    """
    req_json = request.get_json()
    if req_json:
        turtle_name = req_json.get('turtle_name')
        num_of_prints = req_json.get('number_of_prints', config.NUMBER_OF_PRINTS)
        if turtle_name and isinstance(req_json.get('turtle_name'), unicode):
            output = ""
            for i in range(num_of_prints):
                output += "{0}"
            return output.format(turtle_name)
    return "No turtle name received, received json: {0}".format(req_json)


@app.route('/turtle_color', methods=['POST'])
def turtle_color():
    """
    @summary: Get turtle name and color and prints name and color
    """
    req_json = request.get_json()
    if req_json:
        turtle_name = req_json.get('turtle_name', '')
        color = req_json.get('color', '')
        if turtle_name and color:
            if 2 < len(color) < 10 and is_valid_color(color):
                return "Hello {0}. Your color is {1}".format(turtle_name, color)
    else:
        return "invalid json received, received json: {0}".format(req_json)


@app.route('/favorite_turtles', methods=['POST'])
def favorite_turtles():
    """
    @summary: Get turtle name and color and prints name and color
    """
    req_json = request.get_json()
    if req_json:
        fav_turtles = req_json.get('fav_turtles', [])
        if fav_turtles and len(fav_turtles) == 3 and is_valid_turtles(fav_turtles):
            fav_turtles.sort(reverse=True)
            output = {'sorted_turtles': fav_turtles}
            return json.dumps(output)
        else:
            return "invalid turtles received, go to watch channel 6 again"
    else:
        return "invalid json received, received json: {0}".format(req_json)


@app.route('/create_character', methods=['POST'])
def create_character():
    req_json = request.get_json()
    if req_json:
        if type(req_json) is not list:
            req_json = [req_json]

        for character in req_json:
            name = character.get('name', '')
            weapon = character.get('weapon', '')
            role = character.get('role', '')
            color = character.get('color', '')

            if name and weapon and role and color:
                if is_valid_role(role) and is_valid_color(color):
                    # all the check below in real world will be inside our insert function or validation module
                    # in case of using mongodb
                    if len(name) <= 30 and len(weapon) <= 50 and len(role) <= 10 and len(color) <= 10:
                        # In real world i'll create a class to store collection and db instantly or just received
                        # the collection name as parameter
                        character_collection.insert(character)

        return 'inserted to db'
    else:
        return "invalid json received, received json: {0}".format(req_json)


@app.route('/start_a_flight', methods=['PUT'])
def start_a_flight():
    req_json = request.get_json()
    if req_json:
        n_flights = req_json.get('n_flights', 0)
        if 1 <= n_flights <= 3:
            flight_ids = []
            number_of_good_chars = character_collection.count({'role': 'good'})
            number_of_bad_chars = character_collection.count({'role': 'bad'})

            for i in range(n_flights):
                try:
                    good_random_int = random.randint(0, number_of_good_chars - 1)
                    bad_random_int = random.randint(0, number_of_bad_chars - 1)

                    flight_id = uuid.uuid4().__str__()
                    try:
                        good_char = character_collection.find({'role': 'good'}).skip(good_random_int).limit(1).next()
                        bad_char = character_collection.find({'role': 'bad'}).skip(bad_random_int).limit(1).next()
                    except StopIteration:
                        return 'Something went wrong while pulling out character docs, good_random_int: {0},' \
                               'bad_random_int: {1}'.format(good_random_int, bad_random_int)

                    ninja_jobs_collection.insert({'flight_id': flight_id,
                                                  'good_char_id': good_char['_id'],
                                                  'good_char_weapon': good_char['weapon'],
                                                  'bad_char_id': bad_char['_id'],
                                                  'bad_char_weapon': bad_char['weapon']})
                    print "flight id {0} between {1} and {2} is about to start".format(flight_id, good_char['name'],
                                                                                       bad_char['name'])
                    flight_ids.append(flight_id)

                except IndexError:
                    return 'Something wrong with the db, get index error while trying to choose of the the characters'

            return json.dumps({'flight_ids': flight_ids})
        else:
            return "number of flights must be between 1 to 3, received json: {0}".format(req_json)

    else:
        return "invalid json received, received json: {0}".format(req_json)


if __name__ == "__main__":
    app.run(host=config.HTTP_HOST, port=config.HTTP_PORT, threaded=True)
